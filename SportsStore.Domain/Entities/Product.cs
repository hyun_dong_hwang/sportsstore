﻿namespace SportsStore.Domain.Entities
{
    public class Product
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set;  }
        public decimal Price { get; set; }
        public string Category { get; set; }
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is Product == false)
                return false;

            var newProduct = (Product)obj;
            var eq = this.ProductID == newProduct.ProductID;
            return eq;
        }

        public override int GetHashCode()
        {
            return this.ProductID;
        }
    }
}
