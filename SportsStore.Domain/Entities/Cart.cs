﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsStore.Domain.Entities
{
    public class Cart
    {
        public List<Product> Products { get; set; }

        public Cart()
        {
            this.Products = new List<Product>();
        }


        public List<Product> GetUniqueProducts()
        {
            var products = this.Products.Distinct().ToList();
            return products;
        }

        public int GetCountPerProduct(int productId)
        {
            var count = this.Products.Count(p => p.ProductID == productId);
            return count;
        }

        public decimal GetSubTotalPrice(int productId)
        {
            var subTotalPrice = this.Products.Where(p => p.ProductID == productId).Sum(p => p.Price);
            return subTotalPrice;
        }

        public decimal GetTotalPrice()
        {
            var totalPrice = this.Products.Sum(p => p.Price);
            return totalPrice;
        }
    }
}
