﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SportsStore.WebUI.Models;
using SportsStore.WebUI.Controllers;

namespace SportsStore.WebUI.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, ProductListVM vm, Func<int, string> funcGetUrl)
        {
            var sb = new StringBuilder();

            for (int i = 1; i <= vm.PageCount; i++)
            {
                var tag = new TagBuilder("a");
                tag.MergeAttribute("href", funcGetUrl(i));
                tag.InnerHtml = i.ToString();

                if (i == vm.PageIndex)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }

                tag.AddCssClass("btn btn-default");
                sb.Append(tag.ToString());
            }

            var mvcHtmlString = MvcHtmlString.Create(sb.ToString());
            return mvcHtmlString;
        }
    }
}