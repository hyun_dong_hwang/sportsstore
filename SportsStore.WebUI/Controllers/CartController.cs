﻿using SportsStore.Domain.Concrete;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Controllers
{
    public class CartController : Controller
    {
        private EFDbContext _dbContext = new EFDbContext();

        // GET: Cart
        public ActionResult Index(string returnUrl)
        {
            var vm = new CartIndexVM();
            vm.CartObj = _GetCart();
            vm.ReturnUrl = returnUrl;
            return View(vm);
        }

        public RedirectToRouteResult AddToCart(int productId, string returnUrl)
        {
            var product = _dbContext.Products.FirstOrDefault(p => p.ProductID == productId);

            if (product != null)
            {
                _GetCart().Products.Add(product);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToRouteResult RemoveFromCart(int productId, string returnUrl)
        {
            var product = _GetCart().Products.FirstOrDefault(p => p.ProductID == productId);

            if (product != null)
            {
                _GetCart().Products.Remove(product);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public PartialViewResult Summary()
        {
            return PartialView(_GetCart());
        }

        public ViewResult Checkout()
        {
            return View(new CartCheckoutVM());
        }

        [HttpPost]
        public ViewResult Checkout(CartCheckoutVM model)
        {
            if (!this.ModelState.IsValid)
                return View(new CartCheckoutVM());



            var cart = _GetCart();

            if (!cart.Products.Any())
            {
                this.ModelState.AddModelError("", "Sorry, your cart is empty!");
                return View(new CartCheckoutVM());
            }



            using (var smtpClient = new SmtpClient())
            {
                var canUseNaverSmtp = false;

                if (canUseNaverSmtp)
                {
                    smtpClient.EnableSsl = true;
                    smtpClient.Host = "smtp.naver.com";
                    smtpClient.Port = 465;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential("h2d2002", "hhd2002");
                }
                else
                {
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtpClient.PickupDirectoryLocation = @"c:\temp";
                    smtpClient.EnableSsl = false;
                }

                var body = new StringBuilder();
                body.AppendLine("A new order has been submitted");
                body.AppendLine("---");
                body.AppendLine("Items : ");

                var uniProducts = cart.GetUniqueProducts();

                foreach (var p in uniProducts)
                {
                    body.AppendFormat("{0} x {1} (subtotal : {2:c}",
                        cart.GetCountPerProduct(p.ProductID),
                        p.Name,
                        cart.GetSubTotalPrice(p.ProductID));
                }

                body.AppendFormat(" Total order value : {0:c}", cart.GetTotalPrice());
                body.AppendLine("---");
                body.AppendLine("Ship to : ");
                body.AppendLine(model.Name);
                body.AppendLine(model.Line1);
                body.AppendLine(model.Line2 ?? "");
                body.AppendLine(model.Line3 ?? "");
                body.AppendLine(model.City);
                body.AppendLine(model.State ?? "");
                body.AppendLine(model.Country);
                body.AppendLine(model.Zip);
                body.AppendLine("---");
                body.AppendFormat("Gift wrap : {0}", model.GifWrap ? "Yes" : "No");

                var mailMsg = new MailMessage(
                    "h2d2002@naver.com",
                    "h2d2002@naver.com",
                    "New order submitted!",
                    body.ToString());

                smtpClient.Send(mailMsg);
            }

            return View("Completed");
        }

        private Cart _GetCart()
        {
            var cart = (Cart)this.Session["Cart"];

            if (cart == null)
            {
                cart = new Cart();
                this.Session["Cart"] = cart;
            }

            return cart;
        }
    }
}