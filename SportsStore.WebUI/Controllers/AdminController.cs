﻿using SportsStore.Domain.Concrete;
using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private EFDbContext _dbContext = new EFDbContext();

        public ViewResult Create()
        {
            return View("Edit", new Product());
        }

        // GET: Admin
        public ActionResult Index()
        {
            return View(_dbContext.Products);
        }

        public ViewResult Edit(int productId)
        {
            var product = _dbContext.Products.FirstOrDefault(p => p.ProductID == productId);
            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(Product product, HttpPostedFileBase image = null)
        {
            if (!this.ModelState.IsValid)
                return View(product);



            if (product.ProductID == 0)
            {
                _dbContext.Products.Add(product);
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }



            var productFound = _dbContext.Products.FirstOrDefault(p => p.ProductID == product.ProductID);

            if (productFound == null)
                return RedirectToAction("Index");



            productFound.Name = product.Name;
            productFound.Price = product.Price;
            productFound.Category = product.Category;
            productFound.Description = product.Description;

            if (image == null)
            {
                productFound.ImageMimeType = null;
                productFound.ImageData = null;
            }
            else
            {
                productFound.ImageMimeType = image.ContentType;
                productFound.ImageData = new byte[image.ContentLength];
                image.InputStream.Read(productFound.ImageData, 0, image.ContentLength);
            }

            _dbContext.SaveChanges();
            this.TempData["message"] = string.Format("{0} has been saved.", product.Name);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int productId)
        {
            var product = _dbContext.Products.FirstOrDefault(p => p.ProductID == productId);

            if (product != null)
            {
                _dbContext.Products.Remove(product);
                _dbContext.SaveChanges();
                this.TempData["message"] = product.Name + " has been deleted.";
            }

            return RedirectToAction("Index");
        }
    }
}