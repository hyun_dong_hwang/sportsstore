﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsStore.Domain;
using SportsStore.Domain.Concrete;
using SportsStore.WebUI.Models;

namespace SportsStore.WebUI.Controllers
{
    public class ProductController : Controller
    {
        public const int ITEMS_SIZE_PER_PAGE = 4;

        private EFDbContext _dbContext = new EFDbContext();

        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult List(string category, int page = 1)
        {
            var vm = new ProductListVM();

            vm.PageIndex = page;
            vm.Category = category;

            vm.Products = _dbContext.Products.
                Where(p => string.IsNullOrEmpty(category) || p.Category == category).
                OrderBy(p => p.ProductID).
                Skip((page - 1) * ITEMS_SIZE_PER_PAGE).
                Take(ITEMS_SIZE_PER_PAGE);

            vm.PageCount = (int)Math.Ceiling((decimal)
                _dbContext.Products.Where(p => string.IsNullOrEmpty(category) || p.Category == category).Count() /
                ITEMS_SIZE_PER_PAGE);

            return View(vm);
        }

        public FileContentResult GetImage(int productId)
        {
            var product = _dbContext.Products.FirstOrDefault(p => p.ProductID == productId);

            if (product != null)
            {
                return File(product.ImageData, product.ImageMimeType);
            }
            else
            {
                return null;
            }
        }
    }
}