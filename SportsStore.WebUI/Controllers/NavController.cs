﻿using SportsStore.Domain.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsStore.WebUI.Controllers
{
    public class NavController : Controller
    {
        private EFDbContext _dbContext = new EFDbContext();

        // GET: Nav
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Menu(string category = null, bool horizontalLayout = false)
        {
            IEnumerable<string> categories = _dbContext.Products
                .Select(x => x.Category)
                .Distinct()
                .OrderBy(x => x);

            this.ViewBag.category = category;
            string viewName = horizontalLayout ? "MenuHorizontal" : "Menu";
            return PartialView(viewName, categories);
        }
    }
}