﻿using SportsStore.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SportsStore.WebUI.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginVM vm, string returnUrl)
        {
            if (!this.ModelState.IsValid)
                return View();


            var result = FormsAuthentication.Authenticate(vm.UserName, vm.Password);

            if (result)
            {
                FormsAuthentication.SetAuthCookie(vm.UserName, false);
                return Redirect(returnUrl ?? Url.Action("Index", "Admin"));
            }

            return View();
        }
    }
}