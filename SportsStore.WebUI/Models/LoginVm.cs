﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportsStore.WebUI.Models
{
    public class LoginVM
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
