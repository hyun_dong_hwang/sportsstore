﻿using SportsStore.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportsStore.WebUI.Models
{
    public class CartIndexVM
    {
        public Cart CartObj { get; set; }
        
        public string ReturnUrl { get; set; }
    }
}
