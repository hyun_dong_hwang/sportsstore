﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SportsStore.Domain.Entities;
using SportsStore.WebUI.Controllers;

namespace SportsStore.WebUI.Models
{
    public class ProductListVM
    {
        public IEnumerable<Product> Products { get; set; }

        public int PageIndex { get; set; }

        public int PageCount { get; set; }

        public string Category { get; set; }
    }
}